extends KinematicBody2D
class_name Player #, "res://path/to/icon.svg"

# ------------------------------------------- ENUMS and CONSTS
enum State {IDLE, WALK, SCREAM, DIG}
enum AimDir{ UP, DOWN, LEFT, RIGHT}

# ------------------------------------------- EXPORTABLE VARS
export var tile_size : int = 64
export var do_walk_time : float = 1
export var max_energy : int = 10
export var walk_price : int = 2
export var scream_price : int = 3 
export var scare_price : int = 2
export var dig_price : int = 2

# ------------------------------------------- SIGNAL VARS
signal energy_emptied
signal victim_found

# ------------------------------------------- ATTRIBUTE VARS
var can_do_walk : bool = true
var energy : int # Seteada en el ready
var input_action : int #enum
var current_state : int # Enum
var old_state : int # Enum
var aiming_dir : int # Enum
var old_aiming_dir : int # Enum
var available_walk_dir : Array = []

# ------------------------------------------- ONREADY VARS

# ------------------------------------------- LIFECYCLE METHODS
#func _enter_tree(): 

func _ready():
	var player_size = Vector2(tile_size-1,tile_size-1)
	$CollisionShape2D.shape.extents = player_size
	energy = max_energy
	can_do_walk = true

func _process(_delta):
	do_check_state_n_dir_changes()
	if energy > 0:
		do_check_kb_input()
	else:
		emit_signal("energy_emptied")
	z_index = int(get_global_transform().get_origin().y)
	#printt("ZINDEX es: ",z_index)


#func _physics_process(delta):
#func _exit_tree():

# ------------------------------------------- OVERRIDABLE METHODS
		
# ------------------------------------------- CUSTOM METHODS

func do_check_state_n_dir_changes():
	if current_state != old_state:
		old_state = current_state
		fix_animation()
	if aiming_dir != old_aiming_dir:
		old_aiming_dir = aiming_dir
		fix_animation()

func do_check_kb_input():
	if Input.is_action_just_pressed("ui_down"):
		do_walk(Vector2(0,1))
	if Input.is_action_just_pressed("ui_up"):
		do_walk(Vector2(0,-1))
	if Input.is_action_just_pressed("ui_left"):
		do_walk(Vector2(-1,0))
	if Input.is_action_just_pressed("ui_right"):
		do_walk(Vector2(1,0))
	if Input.is_action_just_pressed("ui_accept"):
		do_scream(Vector2(1,0))

func do_walk(dir:Vector2) -> void:
	input_action = State.IDLE
	if energy > 0 and can_do_walk and current_state == State.IDLE:
		if can_move_to(dir):
			current_state = State.WALK
			set_player_aim_direction(dir)
			fix_animation()
			var destination = position + dir.normalized() * tile_size
			$Tween.interpolate_property(self,"position",
				position, destination,do_walk_time,
				Tween.TRANS_LINEAR,Tween.EASE_IN)
			can_do_walk = false
			$Tween.start()
			yield($Tween,"tween_all_completed")
			energy -= walk_price
			can_do_walk = true
			printt(energy, "   energy")
			current_state = State.IDLE

func can_move_to(dir : Vector2) -> bool:
	match(dir):
		Vector2(1,0): 
			return $Move_area/Move_check_right.get_overlapping_bodies().size() <= 1
		Vector2(-1,0): 
			return $Move_area/Move_check_left.get_overlapping_bodies().size() <= 1
		Vector2(0,-1): 
			return $Move_area/Move_check_up.get_overlapping_bodies().size() <= 1
		Vector2(0,1): 
			return $Move_area/Move_check_down.get_overlapping_bodies().size() <= 1
		_: return false

func do_scream(dir:Vector2)->void:
	input_action = State.IDLE
	if current_state == State.IDLE:
		current_state = State.SCREAM
		energy -= scream_price
		set_player_aim_direction(dir)
		fix_animation()
		$AudioStreamPlayer2D.stream = SingletonSfxPlayer.get_random_scream()
		$AudioStreamPlayer2D.play()
		current_state = State.IDLE
#		yield($AudioStreamPlayer2D,"finished")
		var b = $Scream_area.get_overlapping_areas()
		for x in b:
			if x.is_in_group("victima"):
				x.do_scream()
			pass
#		printt("Quiero gritar!!!"," ","Dir: ",dir)
	
func do_dig(dir:Vector2)->void:
	input_action = State.IDLE
	if current_state == State.IDLE:
		current_state = State.DIG
		set_player_aim_direction(dir)
		fix_animation()
		energy -= dig_price
		#printt("Garrá lapala!!!"," ","Dir: ",dir)
		match(dir):
			Vector2(1,0):
				if is_any_victim_in($Dig_area/DA_right): 
					emit_signal("victim_found")
			Vector2(1,-1):
				if is_any_victim_in($Dig_area/DA_up_right): 
					emit_signal("victim_found")
			Vector2(1,1):
				if is_any_victim_in($Dig_area/DA_down_right): 
					emit_signal("victim_found")
			Vector2(0,-1):
				if is_any_victim_in($Dig_area/DA_up): 
					emit_signal("victim_found")
			Vector2(0,1):
				if is_any_victim_in($Dig_area/DA_down): 
					emit_signal("victim_found")
			Vector2(-1,0):
				if is_any_victim_in($Dig_area/DA_left): 
					emit_signal("victim_found")
			Vector2(-1,-1):
				if is_any_victim_in($Dig_area/DA_up_left): 
					emit_signal("victim_found")
			Vector2(-1,1):
				if is_any_victim_in($Dig_area/DA_down_left): 
					emit_signal("victim_found")
		current_state = State.IDLE
	input_action = State.IDLE

func is_any_victim_in(where:Area2D) -> bool:
	var o = where.get_overlapping_areas()
	if o.size() > 0:
		for x in o:
			if x is Area2D:
				if x.is_in_group("victima") && !x._rescued:
					x.do_found()
					return true
	return false

func do_animate_dig(_way):
	pass

func set_player_aim_direction(dir:Vector2):
	match (dir):
		Vector2(0,0): 
			aiming_dir = AimDir.DOWN
		Vector2(0,1): 
			aiming_dir = AimDir.DOWN
			$Scream_area.rotation = PI * (2/4.0) 
		Vector2(1,0): 
			aiming_dir = AimDir.RIGHT
			$Scream_area.rotation = PI * (0/4.0) 
		Vector2(1,1): 
			aiming_dir = AimDir.RIGHT
			$Scream_area.rotation = PI * (1/4.0) 
		Vector2(-1,0):
			aiming_dir = AimDir.LEFT
			$Scream_area.rotation = PI * (4/4.0) 
		Vector2(-1,1): 
			aiming_dir = AimDir.LEFT
			$Scream_area.rotation = PI * (3/4.0) 
		Vector2(-1,-1): 
			aiming_dir = AimDir.UP
			$Scream_area.rotation = PI * (-3/4.0) 
		Vector2(0,-1): 
			aiming_dir = AimDir.UP
			$Scream_area.rotation = PI * (-2/4.0) 
		Vector2(1,-1): 
			aiming_dir = AimDir.UP
			$Scream_area.rotation = PI * (-1/4.0) 

func fix_animation():
	match(current_state):
		State.IDLE:
			match(aiming_dir):
				AimDir.UP: $AnimatedSprite.animation = "IDLE_UP"
				AimDir.RIGHT: $AnimatedSprite.animation = "IDLE_RIGHT"
				AimDir.LEFT: $AnimatedSprite.animation = "IDLE_LEFT"
				AimDir.DOWN: $AnimatedSprite.animation = "IDLE_DOWN"
		State.WALK:
			match(aiming_dir):
				AimDir.UP: $AnimatedSprite.animation = "WALK_UP"
				AimDir.RIGHT: $AnimatedSprite.animation = "WALK_RIGHT"
				AimDir.LEFT: $AnimatedSprite.animation = "WALK_LEFT"
				AimDir.DOWN: $AnimatedSprite.animation = "WALK_DOWN"
	$AnimatedSprite.play()

# ------------------------------------------- SIGNAL METHODS


func _on_scream_swipe_started() -> void:     # GRITAR
	#print("Quiere gritar", current_state, input_action)
	if input_action == State.IDLE:
		input_action = State.SCREAM


func _on_scream_swipe_ended(dir) -> void:
	if input_action == State.SCREAM:
		#print("Empezar grito", current_state,input_action)
		do_scream(dir)


func _on_walk_swipe_started() -> void:   # CAMINAR
	#print("quiere caminar", current_state,input_action)
	if input_action == State.IDLE:
		input_action = State.WALK


func _on_walk_swipe_ended(dir) -> void:
	#print("Empezar a caminar", current_state,input_action)
	if input_action == State.WALK:
		do_walk(dir)


func _on_Dig_pad_swipe_started() -> void:  # PALEAR
	#print("Quiere palear", current_state,input_action)
	if input_action == State.IDLE:
		input_action = State.DIG


func _on_Dig_pad_swipe_ended(dir) -> void:
	if input_action == State.DIG:
		#print("Empezar a palear", current_state,input_action)
		do_dig(dir)

func _on_anim_diggin_animation_finished():
	$anim_diggin.play("idle")
